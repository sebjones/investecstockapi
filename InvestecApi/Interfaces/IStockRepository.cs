﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvestecApi.Entities;

namespace InvestecApi.Interfaces
{
    public interface IStockRepository
    {
        Task<Stock> FindAsync(Guid id);

        Task<Guid?> UpsertAsync(Stock entity, Guid? id = null);

        Task<Stock> FindByNameAsync(string name);
    }
}

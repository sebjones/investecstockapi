﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvestecApi.Entities;

namespace InvestecApi.Interfaces
{
    public interface IStockService
    {
        Task<Guid?> UpsertStockAsync(string name, string price);

        Task<Stock> GetStockByNameAsync(string name);

        Task<double?> GetPrice(string name);
    }
}

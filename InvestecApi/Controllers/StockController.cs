﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using InvestecApi.Entities;
using InvestecApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace InvestecApi.Controllers
{
    [Route("api/[controller]")]
    public class StockController : Controller
    {
        private readonly IStockService stockService;

        public StockController(IStockService stockService)
        {
            this.stockService = stockService;
        }

        [HttpGet("{stock}/price")]
        public async Task<JsonResult> GetPrice(string stock)
        {
            var price = await stockService.GetPrice(stock);
            if (price.HasValue)
            {
                return new JsonResult(price);
            }

            return new JsonResult(NotFound());
        }
    }
}

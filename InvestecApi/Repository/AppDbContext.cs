﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvestecApi.Entities;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace InvestecApi.Repository
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<Stock> Stocks { get; set; }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}

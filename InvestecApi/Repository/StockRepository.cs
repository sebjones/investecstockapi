﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvestecApi.Entities;
using InvestecApi.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InvestecApi.Repository
{
    public class StockRepository : IStockRepository
    {
        private readonly DbContext dbContext;

        private readonly DbSet<Stock> dataSet;

        public StockRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dataSet = dbContext.Set<Stock>();
        }

        public async Task<Stock> FindAsync(Guid id)
        {
            return await dataSet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid?> UpsertAsync(Stock entity, Guid? id = null)
        {
            if (id.HasValue)
            {
                var current = await FindAsync(id.Value);
                if (current == null)
                {
                    return null;
                }

                entity.Id = id.Value;
                entity.Name = current.Name;
                entity.LastUpdated = DateTime.UtcNow;
            }
            else
            {
                dataSet.Add(entity);
            }

            await dbContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<Stock> FindByNameAsync(string name)
        {
            return await dataSet.FirstOrDefaultAsync(x => x.Name == name);
        }
    }
}

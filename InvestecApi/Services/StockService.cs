﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using InvestecApi.Entities;
using InvestecApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace InvestecApi.Services
{
    public class StockService : IStockService
    {
        private readonly IStockRepository stockRepository;

        public StockService(IStockRepository stockRepository)
        {
            this.stockRepository = stockRepository;
        }
        
        public async Task<Guid?> UpsertStockAsync(string name, string price)
        {
            var stock = await GetStockByNameAsync(name);
            if (stock == null)
            {
                stock = new Stock
                {
                    Name = name,
                    Price = Convert.ToDouble(price)
                };
            }
            return await stockRepository.UpsertAsync(stock, stock.Id);
        }

        public async Task<Stock> GetStockByNameAsync(string name)
        {
            return await stockRepository.FindByNameAsync(name);
        }

        public async Task<double?> GetPrice(string name)
        {
            var stock = await GetStockByNameAsync(name);
            if ((DateTime.UtcNow - stock.LastUpdated).TotalSeconds > 5)
            {
                return await GetStockPriceApi(name);
            }

            return stock.Price;
        }

        private async Task<double?> GetStockPriceApi(string name)
        {
            var uri = $"https://api.iextrading.com/1.0/stock/{name}/price";

            var stockApi = new HttpClient();
            HttpResponseMessage response = await stockApi.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var price = await response.Content.ReadAsStringAsync();

                var result = await UpsertStockAsync(name, price);

                return Convert.ToDouble(price);
            }

            return null;
        }
    }
}
